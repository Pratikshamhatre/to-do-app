import {nanoid} from 'nanoid';
import React, {Component} from 'react'
import Item from './Item';

class List extends Component {

    constructor(props) {
        super(props)

        this.state = {
            toDoItem: '',
            toDoList: (JSON.parse(localStorage.getItem('to-do-list')) || [])
        }
    }

    handleOnChange = (event, index) => {

        if (index === 0 || index) {
            const toDoList = [...this.state.toDoList];
            toDoList[index].value = event.target.value;
            this.setState({
                toDoList: toDoList
            })
            localStorage.setItem('to-do-list', JSON.stringify(toDoList))

        }
        else {
            this.setState({
                toDoItem: event.target.value
            })
        }

    }

    handleOnSubmit = (event) => {
        event.preventDefault();
        const toDoList = [...this.state.toDoList];
        if (this.state.toDoItem) {
            toDoList.push({
                id: nanoid(),
                value: this.state.toDoItem
            })
        }
        this.setState({
            toDoItem: '',
            toDoList: toDoList
        })

        console.log(this.state.toDoList)
        localStorage.setItem('to-do-list', JSON.stringify(toDoList))
    }

    handleRemove = (index) => {
        const toDoList = [...this.state.toDoList];
        toDoList.splice(index, 1)
        this.setState({
            toDoItem: '',
            toDoList: toDoList
        })
        localStorage.setItem('to-do-list', JSON.stringify(toDoList))

    }

    render() {

        let listItem="";
        
        if(this.state.toDoList.length){
            
             listItem = this.state.toDoList.map((ele, index) => {
                return <Item key={ele.id} value={ele.value} remove={() => this.handleRemove(index)} change={(event) => this.handleOnChange(event, index)} />
        })
        }
        return (

            
            <div className="text-center ">
                <h1>To Do App</h1>
                {listItem}

                <form onSubmit={(event) => this.handleOnSubmit(event)}>
                    <div className="input-group">

                        <input className="form-control" onChange={this.handleOnChange} value={this.state.toDoItem} type="text" />
                        <div className="input-group-append">

                            <button type="submit" className="btn btn-primary">ADD</button>
                        </div>


                    </div>
                </form>
            </div>
        )
    }
}


export default List;
