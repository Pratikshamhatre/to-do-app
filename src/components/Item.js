import React, {Component} from 'react'

export default class item extends Component {
    render() {
        return (

            <div className="input-group mb-2">
                <input className="form-control" value={this.props.value} onChange={this.props.change} type="text" />
                <span className="input-group-text cursor-pointer" onClick={this.props.remove}>
                    <i className="material-icons cursor-pointer ml-2" >delete</i></span>
            </div>


        )
    }
}
